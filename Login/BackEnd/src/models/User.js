const mongoose = require('mongoose')
const Schema = mongoose.Schema


const Users = new Schema({

    email: { type: String, required: true },
    password: { type: String, maxlength: 255 },

});

module.exports = mongoose.model('Users', Users);