const { Mongoose } = require("mongoose");

module.exports = {
    mutipleMongooseToobject: function (mongooseArray) {
        return mongooseArray.map(mongoose => mongoose.toObject());
    },
    mongooseToobject: function (mongoose) {
        return mongoose ? mongoose.toObject() : mongoose;
    }
};